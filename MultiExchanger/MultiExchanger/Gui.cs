﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Plugins;

namespace MultiExchanger
{
    public partial class Gui : Form
    {
        public Gui()
        {
            InitializeComponent();
        }

        public void GuiSaveData_Refresh()
        {

            textBox1.Text = Main.Settings.ExchangeNPC_Map.ToString() + " "
            + ObjectManager.GetTemplateInfo(Main.Settings.ExchangeNPC_TemplateID).Name + "@"
            + Main.Settings.ExchangeNPC_Location.ToString()
            ;

            numericUpDown1.Value = Convert.ToDecimal(Main.Settings.ExchangeNPC_OptionToUse) + 1;

            if (Main.Settings.ExchangeNPC_NeededItem_ID != 0)
            {
                comboBox3.Text = ObjectManager.GetItemInfo(Main.Settings.ExchangeNPC_NeededItem_ID).Name;
            }
            else
            {
                comboBox3.Text = "None Item Selected";
            }
            if (Main.Settings.ExchangeNPC_ItemToUse_ID != 0)
            {
                comboBox1.Text = ObjectManager.GetItemInfo(Main.Settings.ExchangeNPC_ItemToUse_ID).Name;
            }
            else
            {
                comboBox1.Text = "None Item Selected";
            }
            if (Main.Settings.ExchangeNPC_UseItemAfterwards)
            {
                radioButton1.Checked = true;
                radioButton2.Checked = false;
            }
            else
            {
                radioButton1.Checked = false;
                radioButton2.Checked = true;
            }
            if (Main.Settings.ExchangeNPC_LoadNextProfile)
            {
                radioButton3.Checked = false;
                radioButton4.Checked = true;
            }
            else
            {
                radioButton3.Checked = true;
                radioButton4.Checked = false;
            }
            if (Main.Settings.ExchangeNPC_NextProfile != "" && Main.Settings.ExchangeNPC_NextProfile != "None Profile Selected")
            {
                comboBox4.Text = Main.Settings.ExchangeNPC_NextProfile;
            }
            else
            {
                comboBox4.Text = "None Profile Selected";
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Main.Me.CurrentTarget != null)
            {
                Main.ExchangeNPC_Location_Copy = Main.Me.CurrentTarget.Location3D;
                Main.ExchangeNPC_TemplateID_Copy = Main.Me.CurrentTarget.Template.Id;
                Main.ExchangeNPC_Map_Copy = Main.Me.Map;

                textBox1.Text = Main.ExchangeNPC_Map_Copy.ToString() + " "
                + ObjectManager.GetTemplateInfo(Main.ExchangeNPC_TemplateID_Copy).Name + "@"
                + Main.ExchangeNPC_Location_Copy.ToString()
                ;
            }
            else
            {
                MessageBox.Show("No Target Selected.", "Error: NoTarget");
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                Main.ExchangeNPC_UseItemAfterwards_Copy = true;
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

            if (radioButton2.Checked)
            {
                Main.ExchangeNPC_UseItemAfterwards_Copy = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            InterfaceObject _noItem = new InterfaceObject();
            _noItem.Text = "None Item Selected";
            _noItem.Object = null;
            comboBox1.Items.Add(_noItem);
            foreach (var item in Main.Me.Bags.Where(x => x.IsValid()))
            {
                InterfaceObject _newItem = new InterfaceObject();
                _newItem.Text = item.InternalData.Name;
                _newItem.Object = item.Id;
                comboBox1.Items.Add(_newItem);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            comboBox2.Items.Clear();
            string[] filePaths = Directory.GetFiles(Helper.ProfilesDirectory, "*.xml");
            foreach (var file in filePaths)
            {

                InterfaceObject Config = new InterfaceObject();
                Config.Text = Path.GetFileNameWithoutExtension(file);
                Config.Object = file;
                comboBox2.Items.Add(Config);
            }
        }
        public static void ReviveSaveData()
        {
            Main.ExchangeNPC_TemplateID_Copy = Main.Settings.ExchangeNPC_TemplateID;
            Main.ExchangeNPC_OptionToUse_Copy = Main.Settings.ExchangeNPC_OptionToUse;
            Main.ExchangeNPC_Location_Copy = Main.Settings.ExchangeNPC_Location;
            Main.ExchangeNPC_Map_Copy = Main.Settings.ExchangeNPC_Map;
            Main.ExchangeNPC_UseItemAfterwards_Copy = Main.Settings.ExchangeNPC_UseItemAfterwards;
            Main.ExchangeNPC_ItemToUse_ID_Copy = Main.Settings.ExchangeNPC_ItemToUse_ID;
            Main.ExchangeNPC_NeededItem_ID_Copy = Main.Settings.ExchangeNPC_NeededItem_ID;
            Main.ExchangeNPC_NextProfile_Copy = Main.Settings.ExchangeNPC_NextProfile;
            Main.ExchangeNPC_LoadNextProfile_Copy = Main.Settings.ExchangeNPC_LoadNextProfile;

        }
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedItem != null)
            {
                Main.Settings = Helper.Load<Settings>((string)((InterfaceObject)comboBox2.SelectedItem).Object);
                GuiSaveData_Refresh();
                ReviveSaveData();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {


            Main.Settings.ExchangeNPC_TemplateID = Main.ExchangeNPC_TemplateID_Copy;
            Main.Settings.ExchangeNPC_OptionToUse = Main.ExchangeNPC_OptionToUse_Copy;
            Main.Settings.ExchangeNPC_Location = Main.ExchangeNPC_Location_Copy;
            Main.Settings.ExchangeNPC_Map = Main.ExchangeNPC_Map_Copy;
            Main.Settings.ExchangeNPC_UseItemAfterwards = Main.ExchangeNPC_UseItemAfterwards_Copy;
            Main.Settings.ExchangeNPC_ItemToUse_ID = Main.ExchangeNPC_ItemToUse_ID_Copy;
            Main.Settings.ExchangeNPC_NeededItem_ID = Main.ExchangeNPC_NeededItem_ID_Copy;
            Main.Settings.ExchangeNPC_NextProfile = Main.ExchangeNPC_NextProfile_Copy;
            Main.Settings.ExchangeNPC_LoadNextProfile = Main.ExchangeNPC_LoadNextProfile_Copy;

            saveFileDialog1.Title = "Save Exchanger Profile";
            saveFileDialog1.Filter = "XML files|*.XML";
            saveFileDialog1.InitialDirectory = Helper.ProfilesDirectory;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Helper.Save(saveFileDialog1.FileName, Main.Settings);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (Main.IsRunning)
            {
                Main.IsRunning = false;
                button3.Text = "> > > S T A R T < < <";
                button3.ForeColor = Color.Lime;
                button3.Image = Properties.Resources.media_play_green;
            }
            else
            {
                Main.IsRunning = true;
                button3.Text = "> > > S T O P - E X C H A N G E < < <";
                button3.ForeColor = Color.Red;
                button3.Image = Properties.Resources.PauseNormalRed;
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            Main.ExchangeNPC_OptionToUse_Copy = (uint)numericUpDown1.Value - 1;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((InterfaceObject)comboBox1.SelectedItem).Object != null)
            {
                Main.ExchangeNPC_ItemToUse_ID_Copy = (uint)((InterfaceObject)comboBox1.SelectedItem).Object;
            }
            else
            {
                Main.ExchangeNPC_ItemToUse_ID_Copy = 0;
                Skandia.MessageLog("Exception catched.");
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

            Main.ExchangeNPC_NeededItem_ID_Copy = (uint)((InterfaceObject)comboBox3.SelectedItem).Object;
        }

        private void button6_Click(object sender, EventArgs e)
        {

            comboBox3.Items.Clear();
            InterfaceObject _noItem = new InterfaceObject();
            _noItem.Text = "None Item Selected";
            _noItem.Object = 0;
            comboBox3.Items.Add(_noItem);
            foreach (var item in Main.Me.Bags.Where(x => x.IsValid()))
            {
                InterfaceObject _newItem = new InterfaceObject();
                _newItem.Text = item.InternalData.Name;
                _newItem.Object = item.Id;
                comboBox3.Items.Add(_newItem);
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {

            if (radioButton3.Checked)
            {
                Main.ExchangeNPC_LoadNextProfile_Copy = false;
            }
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {

            if (radioButton4.Checked)
            {
                Main.ExchangeNPC_LoadNextProfile_Copy = true;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {

            comboBox4.Items.Clear();
            InterfaceObject _empty = new InterfaceObject();
            _empty.Text = "No Profile Selected";
            _empty.Object = "No Profile Selected";
            comboBox4.Items.Add(_empty);
            string[] filePaths = Directory.GetFiles(Helper.ProfilesDirectory, "*.xml");
            foreach (var file in filePaths)
            {
                InterfaceObject Config = new InterfaceObject();
                Config.Text = Path.GetFileNameWithoutExtension(file);
                Config.Object = Path.GetFileNameWithoutExtension(file) + ".xml"; ;
                comboBox4.Items.Add(Config);
            }
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            Main.ExchangeNPC_NextProfile_Copy = (string)((InterfaceObject)(comboBox4.SelectedItem)).Object;
        }
        uint helper_x = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Main.Settings.ExchangeNPC_LoadNextProfile && helper_x != 0 && helper_x != Main.Settings.ExchangeNPC_TemplateID)
            {
                GuiSaveData_Refresh();
            }
            helper_x = Main.ExchangeNPC_TemplateID_Copy;
        }
    }
}
