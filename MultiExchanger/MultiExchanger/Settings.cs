﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugins;

namespace MultiExchanger
{
    public class Settings
    {
        public uint ExchangeNPC_TemplateID { get; set; }
        public uint ExchangeNPC_OptionToUse { get; set; }
        public Vector3 ExchangeNPC_Location { get; set; }
        public short ExchangeNPC_Map { get; set; }

        public bool ExchangeNPC_UseItemAfterwards { get; set; }
        public uint ExchangeNPC_ItemToUse_ID { get; set; }
        public uint ExchangeNPC_NeededItem_ID { get; set; }

        public bool ExchangeNPC_LoadNextProfile { get; set; }
        public string ExchangeNPC_NextProfile { get; set; }
        public Settings()
        {
            ExchangeNPC_TemplateID = 0;
            ExchangeNPC_OptionToUse = 0;

            ExchangeNPC_UseItemAfterwards = false;
            ExchangeNPC_ItemToUse_ID = 0;


            ExchangeNPC_Location = new Vector3();
            ExchangeNPC_Map = 0;
            ExchangeNPC_NeededItem_ID = 0;
            ExchangeNPC_LoadNextProfile = false;
            ExchangeNPC_NextProfile = "None Profile Selected";
        }
    }
}
