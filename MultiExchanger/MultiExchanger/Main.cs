﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugins;
using PluginsCommon;
using System.Reflection;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.IO;

namespace MultiExchanger
{
    public class Main : IPlugin
    {

        #region Author, Desc, Name and Version of Plugin
        public string Author
        {
            get
            {
                return "netHox";
            }
        }

        public string Description
        {
            get
            {
                return "Auto Exchange Tool with Moving function.";
            }
        }

        public string Name
        {
            get
            {
                return "MultiExchanger";
            }
        }

        public Version Version
        {
            get { return Assembly.GetExecutingAssembly().GetName().Version; }

        }
        #endregion

        #region Interfaces
        private Thread _guiManager;
        private Gui Gui;
        private void _guiManagement()
        {
            if (Gui == null)
            {
                Gui = new Gui();
            }
            Gui.ShowDialog();
        }
        public void OnButtonClick()
        {
            #region SettingsInterface
            if (Skandia.IsInGame)
            {
                #region SettingsInterface
                // im lazy so I check all these instead...
                if (_guiManager == null ||
                    _guiManager.ThreadState == System.Threading.ThreadState.Unstarted ||
                    _guiManager.ThreadState == System.Threading.ThreadState.Aborted ||
                    _guiManager.ThreadState == System.Threading.ThreadState.Stopped)
                {
                    _guiManager = new Thread(_guiManagement);
                    // GUI must run in a single thread apartment
                    _guiManager.SetApartmentState(ApartmentState.STA);
                    _guiManager.Start();
                }
                else
                {
                    if (Gui != null)
                    {
                        Gui.Invoke((MethodInvoker)delegate
                        {
                            Gui.Close();
                        });
                    }
                }
                #endregion
            }
            else
            {
                MessageBox.Show("[Startup] You're not ingame, Settings UI will can only be started ingame.", "Error");
            }
            #endregion
        }
        #endregion


        public void OnStart()
        {
            if (!Directory.Exists(Helper.ProfilesDirectory))
            {
                Directory.CreateDirectory(Helper.ProfilesDirectory);
            }
            IsRunning = false;
            NPC = null;
            Settings = new Settings();

        }

        public void OnStop(bool off)
        {

        }

        public static uint ExchangeNPC_TemplateID_Copy { get; set; }
        public static uint ExchangeNPC_OptionToUse_Copy { get; set; }
        public static Vector3 ExchangeNPC_Location_Copy { get; set; }
        public static short ExchangeNPC_Map_Copy { get; set; }
        public static bool ExchangeNPC_UseItemAfterwards_Copy { get; set; }
        public static uint ExchangeNPC_ItemToUse_ID_Copy { get; set; }
        public static uint ExchangeNPC_NeededItem_ID_Copy { get; set; }

        public static bool ExchangeNPC_LoadNextProfile_Copy { get; set; }
        public static string ExchangeNPC_NextProfile_Copy { get; set; }


        public static Settings Settings { get; set; }
        public static SkandiaObject NPC { get; set; }
        public static LocalPlayer Me { get; set; }
        public static bool IsRunning { get; set; }
        private static readonly Stopwatch MyTimer = new Stopwatch();
        private static readonly Stopwatch Exchangetimer = new Stopwatch();
        public void Pulse()
        {
            Skandia.Update();
            Me = Skandia.Me;

            if (!MyTimer.IsRunning)
            {
                MyTimer.Start();
            }
            if (MyTimer.ElapsedMilliseconds < 1000)
            {
                return;
            }
            MyTimer.Restart();
            if (!IsRunning || !Skandia.IsInGame)
            {
                return;
            }

            if (!Me.Bags.Any(x => x.IsValid() && x.Id == Settings.ExchangeNPC_NeededItem_ID))
            {
                if (Settings.ExchangeNPC_UseItemAfterwards && Me.Bags.Any(x => x.IsValid() && x.Id == Settings.ExchangeNPC_ItemToUse_ID)
                    && (!Me.Bags.Any(x => x.IsValid() && x.Id == Settings.ExchangeNPC_NeededItem_ID)))
                {
                    int num = 0;
                    using (List<Bag>.Enumerator enumerator = Me.Bags.GetEnumerator())
                    {
                        while (enumerator.MoveNext())
                        {

                            Bag item = enumerator.Current;
                            num++;
                            if (item.Id != 0)
                            {
                                if (item.Id == Settings.ExchangeNPC_ItemToUse_ID && Settings.ExchangeNPC_ItemToUse_ID != 0)
                                {
                                    Skandia.MessageLog("Using Item: " + item.InternalData.Name);
                                    item.Use();
                                    goto Label_EndItem;
                                }

                            }
                        }
                    }

                    Label_EndItem:;
                }
                else
                {
                    Skandia.MessageLog("Done, Autoloading Profile: " + Settings.ExchangeNPC_NextProfile);
                    Settings = Helper.Load<Settings>(Path.Combine(Helper.ProfilesDirectory, Settings.ExchangeNPC_NextProfile));
                    Gui.ReviveSaveData();
                    try
                    {
                        Gui.GuiSaveData_Refresh();
                    }
                    catch
                    {

                    }
                }
            }
            if (!Me.Bags.Any(x => x.IsValid() && x.Id == Settings.ExchangeNPC_NeededItem_ID))
            {
                Skandia.MessageLog("Needed Exchange Item is empty : " + ObjectManager.GetItemInfo(Settings.ExchangeNPC_NeededItem_ID).Name);
                return;
            }
            if(Skandia.Core.Mover.IsIdle() && (Me.Map != Settings.ExchangeNPC_Map || Me.Location3D.Distance(Settings.ExchangeNPC_Location) > 5))
            {
                Skandia.MessageLog("Moving...");
                Skandia.Core.Mover.MoveTo(Settings.ExchangeNPC_TemplateID);
            }
            else
            {
                // Is there.
                var _npc = ObjectManager.ObjectList.FirstOrDefault(x => x.IsValid && x.Template != null && x.Template.Id == Settings.ExchangeNPC_TemplateID);
                if (!Exchangetimer.IsRunning)
                {
                    if (!Skandia.Core.Mover.IsIdle())
                    {
                        Skandia.Core.Mover.Stop();
                    }
                    Exchangetimer.Start();
                    Skandia.MessageLog("Arrived...");
                }
                if (Exchangetimer.ElapsedMilliseconds > 2000)
                {
                    Skandia.MessageLog("Exchanging: " + ObjectManager.GetItemInfo(Settings.ExchangeNPC_NeededItem_ID).Name );
                    Me.OpenNpcDialog(_npc.Guid, Settings.ExchangeNPC_OptionToUse);
                    if (Settings.ExchangeNPC_UseItemAfterwards && Me.Bags.Any(x => x.IsValid() && x.Id == Settings.ExchangeNPC_ItemToUse_ID))
                    {
                        int num = 0;
                        using (List<Bag>.Enumerator enumerator = Me.Bags.GetEnumerator())
                        {
                            while (enumerator.MoveNext())
                            {

                                Bag item = enumerator.Current;
                                num++;
                                if (item.Id != 0)
                                {
                                    if (item.Id == Settings.ExchangeNPC_ItemToUse_ID && Settings.ExchangeNPC_ItemToUse_ID != 0)
                                    {
                                        Skandia.MessageLog("Using Item: " + item.InternalData.Name);
                                        item.Use();
                                        goto Label_EndItem;
                                    }

                                }
                            }
                        }

                        Label_EndItem:;
                    }
                    Exchangetimer.Stop();
                }

            }
                
        }

    }

    public class InterfaceObject
    {
        public string Text { get; set; }
        public object Object { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}
