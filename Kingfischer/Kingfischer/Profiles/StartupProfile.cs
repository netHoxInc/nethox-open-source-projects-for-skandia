﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kingfischer
{
    public class StartupProfile
    {
        public int Language { get; set; }
        public string Kingfishprofile { get; set; }
        public StartupProfile()
        {
            Language = 0;
            Kingfishprofile = "None choosen.";
        }
    }
}
