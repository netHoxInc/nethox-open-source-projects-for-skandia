﻿using Plugins;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kingfischer
{
    public class KingfishProfile
    {
        public BindingList<KingfishData> KingfishList = new BindingList<KingfishData>();
        public uint FishSeller { get; set; }
        public uint Seller { get; set; }
        public uint Baitamount { get; set; }
        public uint BaitID { get; set; }
        public string NormalSellprofile { get; set; }
        public string KFSellprofile { get; set; }
        public Vector3 FishingLocation { get; set; }
        public short FishingMap { get; set; }

        public KingfishProfile()
        {
            KingfishList = new BindingList<KingfishData>();
            FishSeller = 0;
            Seller = 0;
            Baitamount = 0;
            BaitID = 0;
            NormalSellprofile = "";
            KFSellprofile = "";
            FishingLocation = new Vector3();
            FishingMap = 0;
        }
    }
}
