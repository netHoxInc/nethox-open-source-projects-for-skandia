﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugins;
using PluginsCommon;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace Kingfischer
{
    public class MainApp : IPlugin
    {
        public static LocalPlayer Me { get; set; }
        public static bool DebugMode = false;
        public static State CurrentFSMState { get; set; }
        public static State _currentState;
        public static List<State> _states;
        public static bool IsRunning = false;
        public static bool IsFullStopped = false;
        public static KingfishData CurrentKingfish { get; set; }
        // Seller
        public static bool HasSold = false;
        public static bool IsSelling = false;
        public static bool HasSellingBeenDone = false;
        public static bool StartUpComplete = false;
        public static State LastState { get; set; }

        // CopyPastas
        public static Vector3 Kingfish_Location_Copy { get; set; }
        public static short Kingfish_Map_Copy { get; set; }
        public static uint ID_Copy { get; set; }
        public static bool Faketeleport_Copy { get; set; }
        public static uint Faketeleport_SynchronizationTime_Copy { get; set; }
        public static short Faketeleport_Map_Copy { get; set; }
        public static Vector3 Faketeleport_Location_Begin_Copy { get; set; }
        public static Vector3 Faketeleport_Location_End_Copy { get; set; }
        public static TimeSpan Kingfish_Time_Copy { get; set; }
        public static DayOfWeek Kingfish_Day_Copy { get; set; }


        public static bool IsAutomated = false;

        public static KingfishProfile Settings;
        public static StartupProfile StartUpSettings;
        public static bool IsCurrentlyKingfish = false;

        public static void FinnishStartup(bool finnish)
        {
            StartUpComplete = finnish;
        }


        #region Author, Desc, Name and Version of Plugin
        public string Author
        {
            get
            {
                return "netHox";
            }
        }

        public string Description
        {
            get
            {
                return "Automated Kingfishing and normal Fishbotting if none is spawned.";
            }
        }

        public string Name
        {
            get
            {
                return "Auto Kingfisher";
            }
        }

        public Version Version
        {
            get { return Assembly.GetExecutingAssembly().GetName().Version; }

        }
        #endregion
        #region Interfaces
        private Thread _guiManager;
        private KingfischUI Gui;


        private void _guiManagement()
        {
            if (Gui == null)
            {
                Gui = new KingfischUI();
            }
            Gui.ShowDialog();
        }

        private void _Start_Interface()
        {
            #region SettingsInterface
            // im lazy so I check all these instead...
            if (_guiManager == null ||
                _guiManager.ThreadState == System.Threading.ThreadState.Unstarted ||
                _guiManager.ThreadState == System.Threading.ThreadState.Aborted ||
                _guiManager.ThreadState == System.Threading.ThreadState.Stopped)
            {
                _guiManager = new Thread(_guiManagement);
                // GUI must run in a single thread apartment
                _guiManager.SetApartmentState(ApartmentState.STA);
                _guiManager.Start();
            }
            else
            {
                if (Gui != null)
                {
                    Gui.Invoke((MethodInvoker)delegate
                    {
                        Gui.Close();
                    });
                }
            }
            #endregion
            #region Initialized Variables and States
            #endregion
        }
        public void OnButtonClick()
        {
            #region SettingsInterface
            if (Skandia.IsInGame)
            {
                _Start_Interface();
            }
            else
            {
                MessageBox.Show("[Startup] You're not ingame, Settings UI will can only be started ingame.", "Error");
            }
            #endregion
        }
        #endregion

        #region StartUp Void with MainUI Triggering, other Startup things
        public static bool ProfileisLoaded = false;
        public static bool StartUp = true;
        public static bool ArrivedAtKingfish = true;
        public static bool ArrivedAtNormalFishes = true;
        public static bool IsWaitingForNewKingfishSpawn = true;
        public static bool GoSellAfterKingfish = false;
        public static bool GoSellBeforeKingfish = false;

        public static void StartAutomation()
        {
            if (ProfileisLoaded)
            {
                IsRunning = true;
            }
            else
            {
                Log("No Profile has been loaded, unable to start Automation process.");
            }
        }
        public void OnStart()
        {

            if (!Directory.Exists(Helper.Dir_Profiles))
            {
                Directory.CreateDirectory(Helper.Dir_Profiles);
            }
            if (!Directory.Exists(Helper.Dir_StartUpProfiles))
            {
                Directory.CreateDirectory(Helper.Dir_StartUpProfiles);
            }

            Settings = new KingfishProfile();
            StartUpSettings = new StartupProfile();

            _states = new List<State>
            {
                new Move(),
                new Seller(),
                new StartUp(),
                new Fischer(),
                new Kingfisher(),

            };
            _states.Sort();

        }
        #endregion
        #region Stopping Void, Triggers off all UI's before closing to ensure they dont bug on a restart.
        public void OnStop(bool off)
        {
            Skandia.Core.Seller.StopSelling();
            Skandia.Core.Fighter.Stop();
            Skandia.Core.Mover.Stop();
            if (Skandia.Core.GetFishingBotState() == true)
            {
                Skandia.Core.ToggleFishingBot(false);
            }
            if (Skandia.Core.GetArchaeologyBotState() == true)
            {
                Skandia.Core.ToggleArchaeologyBot(false);
            }
        }
        #endregion
        public static TimeSpan Now;
        public static DayOfWeek Today;
        public static bool IsKingfishTimeOver = false;
        #region Main Executive Method, The real process
        public void Pulse()
        {
            Skandia.Update();
            Me = Skandia.Me;
            if (!Skandia.IsInGame || !IsRunning)
            {
                return;
            }
            // Time Sheduler
            #region Get Kingfish by Time
            // Preparationtime: 6mins
            // ServerTime-Sickness Value: 2mins
            var _time = (Settings.KingfishList.FirstOrDefault(x => (x.Kingfish_Day == Today) && (x.Kingfish_Time - new TimeSpan(0, 6, 0) < Now)
            && ((x.Kingfish_Time + new TimeSpan(0, 32, 0)) > Now)));
            if (_time != null)
            {
                // It's Kingfish time
                IsCurrentlyKingfish = true;
                CurrentKingfish = _time;
                IsKingfishTimeOver = false;
                IsWaitingForNewKingfishSpawn = false;
            }
            else
            {
                // It's not or no more Kingfishtime
                IsCurrentlyKingfish = false;
                IsKingfishTimeOver = true;
                IsWaitingForNewKingfishSpawn = true;
            }
            #endregion
            // End Timer
            // Triggers
            #region TriggeringStates
            if (IsKingfishTimeOver)
            {
                GoSellAfterKingfish = true;
            }
            else
            {
                GoSellAfterKingfish = false;
            }
            #endregion
            // Triggers End
            _states.Sort();
            foreach (var state in _states.Where(x => x.NeedToRun))
            {
                if (state != _currentState)
                {
                    if (_currentState != null)
                    {
                        _currentState.Exit();
                    }

                    _currentState = state;
                    _currentState.Enter();
                }
                state.Update();
                break;
            }
        }
        #endregion

        #region Logging
        public static void Log(string input)
        {
            Skandia.MessageLog("[" + CurrentFSMState.Name + "] " + input);
        }
        public static void DebugLog(string input)
        {
            // Only Logs if DebugMode is active.
            if (DebugMode)
            {
                Skandia.MessageLog("[Debug:" + CurrentFSMState.Name + "] " + input);
            }
        }
        #endregion

    }
}
