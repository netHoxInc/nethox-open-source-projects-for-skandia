﻿using Plugins;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kingfischer
{
    public partial class KingfischUI : Form
    {
        public KingfischUI()
        {
            InitializeComponent();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            MainApp.Kingfish_Location_Copy = MainApp.Me.Location3D;
            MainApp.Kingfish_Map_Copy = MainApp.Me.Map;
            textBox1.Text = "Map:" + MainApp.Kingfish_Map_Copy.ToString() + "Loc:" + MainApp.Kingfish_Location_Copy.ToString();
        }

        private void button13_Click(object sender, EventArgs e)
        {

            MainApp.Faketeleport_Location_Begin_Copy = MainApp.Me.Location3D;
            MainApp.Faketeleport_Map_Copy = MainApp.Me.Map;
            textBox2.Text = "Map:" + MainApp.Faketeleport_Map_Copy.ToString() + "Loc:" + MainApp.Faketeleport_Location_Begin_Copy.ToString();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            MainApp.Faketeleport_Location_End_Copy = MainApp.Me.Location3D;
            textBox3.Text = "Map:" + MainApp.Faketeleport_Map_Copy.ToString() + "Loc:" + MainApp.Faketeleport_Location_End_Copy.ToString();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                MainApp.Faketeleport_Copy = true;
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

            if (radioButton2.Checked)
            {
                MainApp.Faketeleport_Copy = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MainApp.Settings.FishingLocation = MainApp.Me.Location3D;
            MainApp.Settings.FishingMap = MainApp.Me.Map;
            textBox5.Text = "Map:" + MainApp.Settings.FishingMap.ToString() + "Loc:" + MainApp.Settings.FishingLocation.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            MainApp.Settings.Seller = MainApp.Me.CurrentTarget.Template.Id;
            MainApp.Settings.FishingMap = MainApp.Me.Map;
            textBox4.Text = "Seller:" + ObjectManager.GetTemplateInfo(MainApp.Settings.Seller).Name + " ID:" + MainApp.Settings.Seller.ToString();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            MainApp.Settings.NormalSellprofile = (string)((KingfischInterfaceObject)(comboBox2.SelectedItem)).Object;

        }

        private void button4_Click(object sender, EventArgs e)
        {
            comboBox2.Items.Clear();
            string[] filePaths = Directory.GetFiles(Helper.Dir_SellerProfiles, "*.akss");
            foreach (var file in filePaths)
            {

                KingfischInterfaceObject Config = new KingfischInterfaceObject();
                Config.Text = Path.GetFileNameWithoutExtension(file);
                Config.Object = Path.GetFileNameWithoutExtension(file);
                comboBox2.Items.Add(Config);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MainApp.Settings.FishSeller = MainApp.Me.CurrentTarget.Template.Id;
            textBox6.Text = "Seller:" + ObjectManager.GetTemplateInfo(MainApp.Settings.FishSeller).Name + " ID:" + MainApp.Settings.FishSeller.ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            comboBox3.Items.Clear();
            string[] filePaths = Directory.GetFiles(Helper.Dir_SellerProfiles, "*.akss");
            foreach (var file in filePaths)
            {

                KingfischInterfaceObject Config = new KingfischInterfaceObject();
                Config.Text = Path.GetFileNameWithoutExtension(file);
                Config.Object = Path.GetFileNameWithoutExtension(file);
                comboBox3.Items.Add(Config);
            }
        }

        private void numericUpDown6_ValueChanged(object sender, EventArgs e)
        {
            MainApp.Settings.Baitamount = Convert.ToUInt32(numericUpDown6.Value);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            comboBox4.Items.Clear();
            for (ushort i = 0; i < ushort.MaxValue; i++)
            {
                var Bait = ObjectManager.GetItemInfo((uint)i);
                if (Bait != null && Bait.Category == ItemDataCategory.FishingBaits)
                {
                    KingfischInterfaceObject AddBait = new KingfischInterfaceObject();
                    AddBait.Text = Bait.Name;
                    AddBait.Object = Bait.Id;
                    comboBox4.Items.Add(AddBait);
                }
            }
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            MainApp.Settings.BaitID = (uint)((KingfischInterfaceObject)comboBox4.SelectedItem).Object;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            MainApp.Settings.KingfishList.Clear();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0)
            {
                return;
            }
            var Kingfisch2Delete = (KingfishData)listBox1.SelectedItem;
            if (Kingfisch2Delete != null)
            {
                MainApp.Settings.KingfishList.Remove(Kingfisch2Delete);
            }
            Skandia.MessageLog("Deleted selected Kingfish in list!");
        }
        private bool Firsttime = false;
        private void button7_Click(object sender, EventArgs e)
        {
            // BUILD KF
            KingfishData NewKingfish = new KingfishData();
            // KF Data
            if (MainApp.Kingfish_Location_Copy != null)
            {
                NewKingfish.Kingfish_Location = MainApp.Kingfish_Location_Copy;
            }
            else
            {
                MessageBox.Show("Re-Check if you entered all Values and Retry!");
                return;
            }
            if (MainApp.Kingfish_Map_Copy != 0)
            {
                NewKingfish.Kingfish_Map = MainApp.Kingfish_Map_Copy;
            }
            else
            {
                MessageBox.Show("Re-Check if you entered all Values and Retry!");
                return;
            }
            if (MainApp.ID_Copy != 0)
            {
                NewKingfish.ID = MainApp.ID_Copy;
            }
            else
            {
                MessageBox.Show("Re-Check if you entered all Values and Retry!");
                return;
            }




            NewKingfish.Faketeleport = MainApp.Faketeleport_Copy;




            if (MainApp.Faketeleport_SynchronizationTime_Copy != 0)
            {
                NewKingfish.Faketeleport_SynchronizationTime = MainApp.Faketeleport_SynchronizationTime_Copy;
            }
            else
            {
                MessageBox.Show("Re-Check if you entered all Values and Retry!");
                return;
            }
            if (MainApp.Faketeleport_Map_Copy != 0)
            {
                NewKingfish.Faketeleport_Map = MainApp.Faketeleport_Map_Copy;
            }
            else
            {
                MessageBox.Show("Re-Check if you entered all Values and Retry!");
                return;
            }
            if (MainApp.Faketeleport_Location_Begin_Copy != null)
            {
                NewKingfish.Faketeleport_Location_Begin = MainApp.Faketeleport_Location_Begin_Copy;
            }
            else
            {
                MessageBox.Show("Re-Check if you entered all Values and Retry!");
                return;
            }
            if (MainApp.Faketeleport_Location_End_Copy != null)
            {
                NewKingfish.Faketeleport_Location_End = MainApp.Faketeleport_Location_End_Copy;
            }
            else
            {
                MessageBox.Show("Re-Check if you entered all Values and Retry!");
                return;
            }
            if (MainApp.Kingfish_Time_Copy != null)
            {
                NewKingfish.Kingfish_Time = MainApp.Kingfish_Time_Copy;
            }
            else
            {
                MessageBox.Show("Re-Check if you entered all Values and Retry!");
                return;
            }
            if (MainApp.Kingfish_Day_Copy != 0)
            {
                NewKingfish.Kingfish_Day = MainApp.Kingfish_Day_Copy;
            }
            else
            {
                MessageBox.Show("Re-Check if you entered all Values and Retry!");
                return;
            }
            // ADD KF
            MainApp.Settings.KingfishList.Add(NewKingfish);

            // Reset New KF
            MainApp.Kingfish_Location_Copy = Vector3.Zero;
            MainApp.Kingfish_Map_Copy = 0;
            MainApp.ID_Copy = 0;
            MainApp.Faketeleport_Copy = false;
            MainApp.Faketeleport_SynchronizationTime_Copy = 0;
            MainApp.Faketeleport_Map_Copy = 0;
            MainApp.Faketeleport_Location_Begin_Copy = Vector3.Zero; ;
            MainApp.Faketeleport_Location_End_Copy = Vector3.Zero;
            MainApp.Kingfish_Time_Copy = new TimeSpan(0,0,0);
            MainApp.Kingfish_Day_Copy = 0;
            comboBox1.Text = "";
            comboBox2.Text = "";
            comboBox3.Text = "";
            comboBox4.Text = "";
            comboBox5.Text = "";
            numericUpDown1.Value = 0;
            numericUpDown2.Value = 0;
            numericUpDown3.Value = 0;
            numericUpDown4.Value = 0;
            numericUpDown6.Value = 0;
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            MessageBox.Show("Added Kingfish, resetted Page for new Entry.");
            if (!Firsttime)
            {
                listBox1.DataSource = MainApp.Settings.KingfishList;
            }

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            MainApp.IsRunning = true;
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {

            MainApp.IsRunning = false;
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if(MainApp.IsFullStopped == true)
            {
                MainApp.IsFullStopped = false;
                toolStripButton3.Text = "Pause";
                toolStripButton3.Image = Properties.Resources.PauseNormalRed;
            }
            else
            {
                MainApp.IsFullStopped = true;
                toolStripButton3.Text = "Resume";
                toolStripButton3.Image = Properties.Resources.media_play_green;
            }
        }

        private void toolStripComboBox1_Click(object sender, EventArgs e)
        {
            if (File.Exists((string)((KingfischInterfaceObject)toolStripComboBox1.SelectedItem).Object))
            {
                MainApp.Settings = Helper.Load<KingfishProfile>((string)((KingfischInterfaceObject)toolStripComboBox1.SelectedItem).Object);
                Skandia.MessageLog("Loaded Profile: " + (string)((KingfischInterfaceObject)toolStripComboBox1.SelectedItem).Object);
                listBox1.DataSource = MainApp.Settings.KingfishList;
            }
            else
            {
                Skandia.MessageLog("No Profile to load has been found.");
            }
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            toolStripComboBox1.Items.Clear();
            string[] filePaths2 = Directory.GetFiles(Helper.Dir_Profiles, "*.xml");
            foreach (var file in filePaths2)
            {
                KingfischInterfaceObject Config = new KingfischInterfaceObject();
                Config.Text = Path.GetFileNameWithoutExtension(file);
                Config.Object = file;
                toolStripComboBox1.Items.Add(Config);
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Title = "Save Kingfish Setting File";
            saveFileDialog1.Filter = "XML files|*.XML";
            saveFileDialog1.InitialDirectory = Helper.Dir_Profiles;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Helper.Save(saveFileDialog1.FileName, MainApp.Settings);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button14_Click(object sender, EventArgs e)
        {
            // Find method
            foreach (var dbEntry in KingFishDatabase.FishKingIdentList)
            {
                KingfischInterfaceObject newEntry = new KingfischInterfaceObject();
                newEntry.Text = ObjectManager.GetTemplateInfo(dbEntry.Key).Name + " on " + ObjectManager.GetMapInfo(dbEntry.Value).Name;
                newEntry.Object = dbEntry.Key;
                comboBox1.Items.Add(newEntry);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            MainApp.ID_Copy = (uint)((KingfischInterfaceObject)comboBox1.SelectedItem).Object;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (MainApp._currentState != null)
            {
                toolStripLabel3.Text = MainApp._currentState.Name;
            }
        }
    }
}
