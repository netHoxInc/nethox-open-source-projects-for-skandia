﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kingfischer
{
    public abstract class State : IComparable<State>, IComparer<State>
    {
        public abstract string Name { get; }

        public abstract int Priority { get; }

        public abstract bool NeedToRun { get; }

        public int CompareTo(State other)
        {
            return -Priority.CompareTo(other.Priority);
        }

        public int Compare(State x, State y)
        {
            return -x.Priority.CompareTo(y.Priority);
        }

        public abstract void Enter();

        public abstract void Update();

        public abstract void Exit();
    }
}
