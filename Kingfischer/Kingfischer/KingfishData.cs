﻿using Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kingfischer
{
    public class KingfishData
    {
        public Vector3 Kingfish_Location { get; set; }
        public short Kingfish_Map { get; set; }
        public uint ID { get; set; }
        public bool Faketeleport { get; set; }
        public uint Faketeleport_SynchronizationTime { get; set; }
        public short Faketeleport_Map { get; set; }
        public Vector3 Faketeleport_Location_Begin { get; set; }
        public Vector3 Faketeleport_Location_End { get; set; }
        public TimeSpan Kingfish_Time { get; set; }
        public DayOfWeek Kingfish_Day { get; set; }

        public override string ToString()
        {
            YesNoBool UseFakePort = new YesNoBool();
            UseFakePort.ImportedBool = Faketeleport;

            string output = "[" + Kingfish_Day.ToString() + ", " + Kingfish_Time.ToString() + "][" 
                + ObjectManager.GetTemplateInfo(ID).Name + "][" + Kingfish_Location.ToString() + "]";
            if (Faketeleport)
            {
                output = output +
                "Use Fakeport[Begin:" + Faketeleport_Location_Begin.ToString() + ", End:" + Faketeleport_Location_End.ToString() + "]";
            }
            return output;

        }
    }
}
