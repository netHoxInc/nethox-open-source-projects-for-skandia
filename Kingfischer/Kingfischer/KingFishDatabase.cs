﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kingfischer
{
    public class KingFishDatabase
    {
        public static Dictionary<uint, uint> FishKingIdentList = new Dictionary<uint, uint>
        {
            {55012,2 },//"Ocean Tyrant,Port Skandia"},
            {55013,3 },//"Shadow of the Deep,Helonia Coast"},
            {55014,4 },//"Golden Blossom,Crescent Hill"},
            {55015,5 },//"Ancient Witness,Cactakara Forest"},
            {55016,6 },//"Dark Crystal,Demarech Mines"},
            {55017,7 },//"Hot-Tempered Overlord,Triatio Highlands"},
            {55018,8 },//"Defender of the Marsh,Candeo Marsh"},
            {55019,9 },//"Rainbow Caller,Ventos Prairie"},
            {55020,10 },//"Deep Waterfall Ghost,Oblitus Wood"},
            {55021,11 },//"Sand-Spitting Hermit,Star Sand Desert"},
            {55041,16 },//"Eternal Wisdom Elder,Vulture's Vale"},
            {55106,12 },//"Fragrant Monstrosity,Rainmist Reach"},
            {55107,13 },//"Lake Lurker,Emerald Marsh"},
            {55108,18 },//"Unidentified Foreign Creature,Starstruck Plateau"},
            {55109,19 },//"Eternal Glacier,Silent Ice Field"},
            {55118,21 },//"Shadow Lancer,Port Morton"},
            {55119,22 },//"Submerged Starlight,Candetonn Hill"},
            {55120,23 },//"Crimson Aquadevil,Viridian Steppe"},
            {55121,24 },//"Khaz Walid,Desolate Valley"},
            {55185,25 },//"Eternal Epicurean,Tanglevine Cascades"},
            {55186,26 },//"Silent Hunter,Sunhunter's Vale"},
        };
    }

}
