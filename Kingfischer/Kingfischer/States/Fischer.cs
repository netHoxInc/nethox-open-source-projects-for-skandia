﻿using Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kingfischer
{
    public class Fischer : State
    {
        public override string Name
        {
            get
            {
                return "Fisher";
            }
        }

        public override bool NeedToRun
        {
            get
            {
                return !MainApp.IsCurrentlyKingfish
                    && MainApp.IsWaitingForNewKingfishSpawn
                    && MainApp.IsRunning
                    ;
            }
        }

        public override int Priority
        {
            get
            {
                return 3;
            }
        }

        public override void Enter()
        {
            MainApp.Log("Seeking Place...");
        }

        public override void Exit()
        {
            Skandia.Core.ToggleFishingBot(false);
            MainApp.Log("New Kingfish spawn coming soon... preparing...");
        }

        public override void Update()
        {
            // Check place
            if (MainApp.Me.Map == MainApp.Settings.FishingMap || MainApp.Me.Location3D.Distance(MainApp.Settings.FishingLocation) < 3)
            {
                Skandia.Core.ToggleFishingBot(true);
            }
            else
            {
                Skandia.Core.ToggleFishingBot(false);
                MainApp.Log("Not in Place yet.");
            }
        }
    }
}
