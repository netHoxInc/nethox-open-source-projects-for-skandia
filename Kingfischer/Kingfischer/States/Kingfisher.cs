﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugins;

namespace Kingfischer
{
    public class Kingfisher : State
    {
        public override string Name
        {
            get
            {
                return "Kingfisher";
            }
        }

        public override bool NeedToRun
        {
            get
            {
                return MainApp.IsCurrentlyKingfish 
                    && MainApp.IsRunning
                    && MainApp.ArrivedAtKingfish
                    && !MainApp.GoSellAfterKingfish
                    ;
            }
        }

        public override int Priority
        {
            get
            {
                return 10;
            }
        }

        public override void Enter()
        {
            MainApp.Log("Searching Kingfish...");
        }

        public override void Exit()
        {
            MainApp.GoSellAfterKingfish = true;
            MainApp.Me.StopFishing();
            MainApp.Log("Done with Kingfish, moving to Seller!");
        }

        public override void Update()
        {
            // Search KF
            var _kf = ObjectManager.ObjectList.FirstOrDefault(x => x.IsValid && x.Template != null && x.Template.Id == MainApp.CurrentKingfish.ID);
            if (_kf != null)
            {
                MainApp.Me.StarFishing(_kf.Guid);
            }
            else
            {
                MainApp.Me.StopFishing();
                MainApp.Log("Could not find a Kingfish yet.");
            }
        }
    }
}
