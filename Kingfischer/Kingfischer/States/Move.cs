﻿using Plugins;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kingfischer
{
    public class Move : State
    {
        public override string Name
        {
            get
            {
                return "Move";
            }
        }

        public override bool NeedToRun
        {
            get
            {
                return 
                    (MainApp.Me.Location3D.Distance(MainApp.CurrentKingfish.Kingfish_Location) > 5  // Not near
                    || MainApp.Me.Map != MainApp.CurrentKingfish.Kingfish_Map) // Not Same Map

                    && MainApp.HasSellingBeenDone
                    && MainApp.IsCurrentlyKingfish // Is Kingfish running
                    && MainApp.IsRunning // Is Plugin Running
                    && !MainApp.ArrivedAtKingfish
                    ;
            }
        }

        public override int Priority
        {
            get
            {
                return 9;
            }
        }


        public override void Enter()
        {
            MainApp.Log("Entering Move State.");
            FakeportSync.Reset();
        }

        public override void Exit()
        {
            MainApp.Log("Exiting Move State.");
            FakeportSync.Stop();
        }
        public bool ArrivedBegin = false;
        public bool ArrivedEnd = false;
        private static readonly Stopwatch FakeportSync = new Stopwatch();
        public override void Update()
        {
            if (!MainApp.CurrentKingfish.Faketeleport)
            {
                // Normal Moving
                if ((MainApp.Me.Location3D.Distance(MainApp.CurrentKingfish.Kingfish_Location) > 3  // Not near
                        || MainApp.Me.Map != MainApp.CurrentKingfish.Kingfish_Map) // Not same Map
                        && Skandia.Core.Mover.IsIdle())
                {
                    Skandia.Core.Mover.MoveAt(MainApp.CurrentKingfish.Kingfish_Location, MainApp.CurrentKingfish.Kingfish_Map, 1);
                    MainApp.Log("Move to Kingfish Location: " + MainApp.CurrentKingfish.Kingfish_Location);
                }
                else
                {
                    // Start Kingfisher State
                    MainApp.ArrivedAtKingfish = true;
                }
            }
            else
            {
                // Moving with Faketeleport Waypoints.
                if ((MainApp.Me.Location3D.Distance(MainApp.CurrentKingfish.Faketeleport_Location_Begin) > 3  // Not near
                        || MainApp.Me.Map != MainApp.CurrentKingfish.Faketeleport_Map) // Not same Map
                        && Skandia.Core.Mover.IsIdle() && !ArrivedBegin)
                {
                    Skandia.Core.Mover.MoveAt(MainApp.CurrentKingfish.Faketeleport_Location_Begin, MainApp.CurrentKingfish.Faketeleport_Map, 1);
                    MainApp.Log("Move to Faketeleport-Start Location: " + MainApp.CurrentKingfish.Faketeleport_Location_Begin);
                }
                else
                {
                    // Arrived at Faketeleport_Location_Begin
                    // Initiating Fakeport.
                    if (!FakeportSync.IsRunning)
                    {
                        FakeportSync.Start();
                    }
                    if (FakeportSync.ElapsedMilliseconds < MainApp.CurrentKingfish.Faketeleport_SynchronizationTime)
                    {
                        MainApp.Log("[Sync] Waiting! [Current Time = " + FakeportSync.ElapsedMilliseconds.ToString() + " of " + MainApp.CurrentKingfish.Faketeleport_SynchronizationTime.ToString() + "ms]");

                        return;
                    }
                    else
                    {
                        ArrivedBegin = true;
                    }

                }

                if ((MainApp.Me.Location3D.Distance(MainApp.CurrentKingfish.Faketeleport_Location_Begin) > 3  // Not near
                        || MainApp.Me.Map != MainApp.CurrentKingfish.Faketeleport_Map) // Not same Map
                        && Skandia.Core.Mover.IsIdle() && ArrivedBegin && !ArrivedEnd)
                {

                    MainApp.Log("Using Fakeport.");
                    MainApp.Me.SetLocation(MainApp.CurrentKingfish.Faketeleport_Location_End);
                    ArrivedEnd = true;
                }
                else
                {
                    if (ArrivedEnd && 
                        (MainApp.Me.Location3D.Distance(MainApp.CurrentKingfish.Kingfish_Location) > 3  // Not near KF
                        || MainApp.Me.Map != MainApp.CurrentKingfish.Kingfish_Map)) // Not same Map
                    {
                        Skandia.Core.Mover.MoveAt(MainApp.CurrentKingfish.Kingfish_Location, MainApp.CurrentKingfish.Kingfish_Map, 1);
                        MainApp.Log("Move to Kingfish Location: " + MainApp.CurrentKingfish.Kingfish_Location);
                    }
                    else
                    {
                        // Start Kingfisher State
                        MainApp.ArrivedAtKingfish = true;
                    }
                }

            }
        }
    }
}
