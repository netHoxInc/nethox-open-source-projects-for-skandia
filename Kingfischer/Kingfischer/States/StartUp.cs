﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kingfischer
{
    public class StartUp : State
    {

        public override string Name
        {
            get
            {
                return "StartUp";
            }
        }

        public override bool NeedToRun
        {
            get
            {
                return MainApp.StartUp == true;
            }
        }

        public override int Priority
        {
            get
            {
                return 11;
            }
        }

        public override void Enter()
        {
            MainApp.Log("Initializing!");
        }

        public override void Exit()
        {
            MainApp.Log("Initialized!");
        }

        public override void Update()
        {
            MainApp.StartUp = false;
        }
    }
}
