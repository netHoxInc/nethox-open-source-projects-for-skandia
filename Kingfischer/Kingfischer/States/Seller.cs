﻿using Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kingfischer
{
    public class Seller : State
    {
        public override string Name
        {
            get
            {
                return "Seller";
            }
        }

        public override bool NeedToRun
        {
            get
            {
                return !MainApp.HasSellingBeenDone 
                    && MainApp.IsCurrentlyKingfish                    
                    ;
            }
        }

        public override int Priority
        {
            get
            {
                return 8;
            }
        }

        public override void Enter()
        {
            MainApp.Log("Entering Seller State.");
        }

        public override void Exit()
        {
            StartSellingNormal = false;
            StartedSellingNormal = false;
            NormalSellerIsDone = false;
            FishingSellerIsDone = false;
            MainApp.Log("Exiting Seller State.");
        }

        public static bool StartSellingNormal = false;
        public static bool StartedSellingNormal = false;
        public static bool StartSellingFisher = false;
        public static bool StartedSellingFisher = false;
        public static bool NormalSellerIsDone = false;
        public static bool FishingSellerIsDone = false;
        public static bool IsSellingBeforeKingfish = true;

        public override void Update()
        {
            // Check if Selling happens before or after a Kingfish to determine Baitbuying or not.
            if (IsSellingBeforeKingfish)
            {
                #region Sellerbehavior before each Kingfish, including Baitbuying
                if (!NormalSellerIsDone)
                {
                    if (ObjectManager.GetMapInfo(1).Name != ObjectManager.GetTemplateInfo(MainApp.Settings.Seller).MapName && Skandia.Core.Mover.IsIdle() && StartSellingNormal == false)
                    {
                        // Not moving and not in Navea, proceed to Move.
                        Skandia.Core.Mover.MoveTo(MainApp.Settings.Seller);
                    }
                    else
                    {
                        // Is moving, checking thing or whatsover
                        var _seller = ObjectManager.ObjectList.FirstOrDefault(x => x.IsValid && x.Template != null & x.Template.Id == MainApp.Settings.Seller);
                        if (_seller != null && _seller.Location3D.Distance(MainApp.Me.Location3D) < 5)
                        {
                            Skandia.Core.Mover.Stop();
                            StartSellingNormal = true;
                        }
                    }
                    if (Skandia.Core.Seller.IsIdle() && !StartedSellingNormal && StartSellingNormal)
                    {
                        Skandia.Core.Seller.StartSelling(MainApp.Settings.NormalSellprofile, MainApp.Settings.Seller);
                    }
                    if (Skandia.Core.Seller.IsIdle() && StartedSellingNormal && StartSellingNormal)
                    {
                        NormalSellerIsDone = true;
                    }
                }
                else
                {
                    // Proceed next Seller, KF Seller.
                    if (!FishingSellerIsDone)
                    {
                        // Go Fishseller
                        // Check if pre or after KF to decide for Buying Baits or not.
                        if (Skandia.Core.Mover.IsIdle() && StartSellingFisher == false)
                        {
                            // Not moving, proceed to Move.
                            Skandia.Core.Mover.MoveTo(MainApp.Settings.FishSeller);
                        }
                        else
                        {
                            // Is moving, checking thing or whatsover
                            var _seller = ObjectManager.ObjectList.FirstOrDefault(x => x.IsValid && x.Template != null & x.Template.Id == MainApp.Settings.FishSeller);
                            if (_seller != null && _seller.Location3D.Distance(MainApp.Me.Location3D) < 5)
                            {
                                Skandia.Core.Mover.Stop();
                                StartSellingFisher = true;
                            }
                        }
                        if (Skandia.Core.Seller.IsIdle() && !StartedSellingFisher && StartSellingFisher)
                        {
                            Skandia.Core.Seller.StartSelling(MainApp.Settings.KFSellprofile, MainApp.Settings.FishSeller);
                        }
                        if (Skandia.Core.Seller.IsIdle() && StartedSellingFisher && StartSellingFisher)
                        {
                            FishingSellerIsDone = true;
                        }
                    }
                    else
                    {
                        // Buy Baits if PRE Kingfish
                        // Do we got enough Baits?
                        if (MainApp.Me.Bags.Any(x => x.Id == MainApp.Settings.BaitID && x.Amount >= MainApp.Settings.Baitamount) // Invtory
                            || MainApp.Me.EquippedItems.Any(x => x.Id == MainApp.Settings.BaitID && x.Amount >= MainApp.Settings.Baitamount)) // Equipped
                        {
                            // We already got enough Baits. Proceeding to KF.
                            // Equip Baits
                            if (!MainApp.Me.EquippedItems.Any(x => x.Id == MainApp.Settings.BaitID))
                            {
                                foreach (var Item in MainApp.Me.Bags.Where(x => x.Id == MainApp.Settings.BaitID))
                                {
                                    Skandia.MessageLog("Equipping Baits.");
                                    Item.Use();
                                }
                            }
                            else
                            {
                                // Equipped, go KF
                                // End Seller State, Transiction to Kingfisch
                                MainApp.HasSellingBeenDone = true;
                            }

                        }
                        else
                        {
                            // We shall buy some Baits.
                            var _npc = ObjectManager.ObjectList.FirstOrDefault(x => x.IsValid && x.Template != null && x.Template.Id == MainApp.Settings.FishSeller);
                            if (MainApp.Me.IsShopWindowOpen())
                            {
                                // Window is opened, Buy Baits now.
                                // De-Equip if Equipped.
                                if (MainApp.Me.EquippedItems.Any(x => x.Id == MainApp.Settings.BaitID))
                                {
                                    foreach (var Item in MainApp.Me.Bags.Where(x => x.Id == MainApp.Settings.BaitID))
                                    {
                                        Skandia.MessageLog("Deequipping Baits.");
                                        Item.Use();
                                    }
                                }
                                else
                                {
                                    // Calculate how much are needed.
                                    uint helper = 0;
                                    uint buyingamount = 0;
                                    foreach (var Item in MainApp.Me.Bags.Where(x => x.Id == MainApp.Settings.BaitID))
                                    {
                                        if (Item != null)
                                        {
                                            helper = helper + Item.Amount;
                                        }
                                    }
                                    buyingamount = MainApp.Settings.Baitamount;
                                    if (helper > 0)
                                    {
                                        buyingamount = buyingamount - helper;
                                    }
                                    if (buyingamount > 0)
                                    {
                                        // Buy needed Baits
                                        if (!MainApp.Me.Bags.Any(x => x.Id == MainApp.Settings.BaitID && x.Amount >= MainApp.Settings.Baitamount)) // Invtory
                                        {
                                            MainApp.Me.BuyItemByItemId(MainApp.Settings.BaitID, buyingamount);
                                        }
                                        else
                                        {
                                            // Equip Baits
                                            if (!MainApp.Me.EquippedItems.Any(x => x.Id == MainApp.Settings.BaitID))
                                            {
                                                foreach (var Item in MainApp.Me.Bags.Where(x => x.Id == MainApp.Settings.BaitID))
                                                {
                                                    Skandia.MessageLog("Equipping Baits.");
                                                    Item.Use();
                                                }
                                            }
                                            else
                                            {
                                                // Equipped, go KF
                                                // End Seller State, Transiction to Kingfisch
                                                MainApp.HasSellingBeenDone = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // Enough Baits
                                    }
                                }
                            }
                            else
                            {
                                // No Window, open it.
                                if (_npc != null) // Check possible null
                                {
                                    MainApp.Me.OpenNpcDialog(_npc.Guid, 2);
                                }
                            }
                        }

                        // End Seller State, Transiction to Kingfisch
                        MainApp.HasSellingBeenDone = true;
                    }
                }
#endregion
            }
            else
            {
                #region Sellerbehavior after each Kingfish
                if (!NormalSellerIsDone)
                {
                    if (ObjectManager.GetMapInfo(1).Name != ObjectManager.GetTemplateInfo(MainApp.Settings.Seller).MapName && Skandia.Core.Mover.IsIdle() && StartSellingNormal == false)
                    {
                        // Not moving and not in Navea, proceed to Move.
                        Skandia.Core.Mover.MoveTo(MainApp.Settings.Seller);
                    }
                    else
                    {
                        // Is moving, checking thing or whatsover
                        var _seller = ObjectManager.ObjectList.FirstOrDefault(x => x.IsValid && x.Template != null & x.Template.Id == MainApp.Settings.Seller);
                        if (_seller != null && _seller.Location3D.Distance(MainApp.Me.Location3D) < 5)
                        {
                            Skandia.Core.Mover.Stop();
                            StartSellingNormal = true;
                        }
                    }
                    if (Skandia.Core.Seller.IsIdle() && !StartedSellingNormal && StartSellingNormal)
                    {
                        Skandia.Core.Seller.StartSelling(MainApp.Settings.NormalSellprofile, MainApp.Settings.Seller);
                    }
                    if (Skandia.Core.Seller.IsIdle() && StartedSellingNormal && StartSellingNormal)
                    {
                        NormalSellerIsDone = true;
                    }
                }
                else
                {
                    // Proceed next Seller, KF Seller.
                    if (!FishingSellerIsDone)
                    {
                        // Go Fishseller
                        // Check if pre or after KF to decide for Buying Baits or not.
                        if (Skandia.Core.Mover.IsIdle() && StartSellingFisher == false)
                        {
                            // Not moving, proceed to Move.
                            Skandia.Core.Mover.MoveTo(MainApp.Settings.FishSeller);
                        }
                        else
                        {
                            // Is moving, checking thing or whatsover
                            var _seller = ObjectManager.ObjectList.FirstOrDefault(x => x.IsValid && x.Template != null & x.Template.Id == MainApp.Settings.FishSeller);
                            if (_seller != null && _seller.Location3D.Distance(MainApp.Me.Location3D) < 5)
                            {
                                Skandia.Core.Mover.Stop();
                                StartSellingFisher = true;
                            }
                        }
                        if (Skandia.Core.Seller.IsIdle() && !StartedSellingFisher && StartSellingFisher)
                        {
                            Skandia.Core.Seller.StartSelling(MainApp.Settings.KFSellprofile, MainApp.Settings.FishSeller);
                        }
                        if (Skandia.Core.Seller.IsIdle() && StartedSellingFisher && StartSellingFisher)
                        {
                            FishingSellerIsDone = true;
                        }
                    }
                    else
                    {
                        // End Seller State, Transiction to Kingfisch
                        MainApp.HasSellingBeenDone = true;
                    }
                }
                #endregion
            }
        }
    }
}
