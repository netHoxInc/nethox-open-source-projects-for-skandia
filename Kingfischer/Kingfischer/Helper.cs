﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace Kingfischer
{
    public class Helper
    {
        #region Directory Strings
        public static string Dir_Profiles
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                // ReSharper disable once AssignNullToNotNullAttribute
                return Path.Combine(Directory.GetParent(Path.GetDirectoryName(path)).FullName, "profiles", "plugins", "Kingfischer");
            }
        }
        public static string Dir_SellerProfiles
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                // ReSharper disable once AssignNullToNotNullAttribute
                return Path.Combine(Directory.GetParent(Path.GetDirectoryName(path)).FullName, "profiles", "seller");
            }
        }
        public static string Dir_StartUpProfiles
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                // ReSharper disable once AssignNullToNotNullAttribute
                return Path.Combine(Directory.GetParent(Path.GetDirectoryName(path)).FullName, "profiles", "plugins", "Kingfischer", "StartUp Profiles");
            }
        }
        #endregion
        #region Loading and Saving Profiles
        public static T Load<T>(string file)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var reader = XmlReader.Create(Path.Combine(Dir_Profiles, file)))
            {
                return (T)serializer.Deserialize(reader);
            }
        }
        public static T StartUpLoad<T>(string file)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var reader = XmlReader.Create(Path.Combine(Dir_StartUpProfiles, file)))
            {
                return (T)serializer.Deserialize(reader);
            }
        }

        public static void Save<T>(string file, T instance)
        {
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            var serializer = new XmlSerializer(typeof(T));
            using (var writer = new StreamWriter(Path.Combine(Dir_Profiles, file)))
            {
                using (var xmlWriter = XmlWriter.Create(writer, new XmlWriterSettings { OmitXmlDeclaration = true, Indent = true, IndentChars = "\t" }))
                {
                    serializer.Serialize(xmlWriter, instance, ns);
                }
            }
        }
        public static void StartUpSave<T>(string file, T instance)
        {
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            var serializer = new XmlSerializer(typeof(T));
            using (var writer = new StreamWriter(Path.Combine(Dir_StartUpProfiles, file)))
            {
                using (var xmlWriter = XmlWriter.Create(writer, new XmlWriterSettings { OmitXmlDeclaration = true, Indent = true, IndentChars = "\t" }))
                {
                    serializer.Serialize(xmlWriter, instance, ns);
                }
            }
        }
        #endregion
        #region HotKeys
        [Flags]
        public enum KeyModifiers
        {
            None = 0,
            Alt = 1,
            Control = 2,
            Shift = 4,
            Windows = 8,
            NoRepeat = 16384,
        }

        public static KeyModifiers GetModifiers(Keys keydata, out Keys key)
        {
            key = keydata;
            var modifers = KeyModifiers.None;

            if ((keydata & Keys.Control) == Keys.Control)
            {
                modifers |= KeyModifiers.Control;

                key = keydata ^ Keys.Control;
            }

            if ((keydata & Keys.Shift) == Keys.Shift)
            {
                modifers |= KeyModifiers.Shift;
                key = key ^ Keys.Shift;
            }

            if ((keydata & Keys.Alt) == Keys.Alt)
            {
                modifers |= KeyModifiers.Alt;
                key = key ^ Keys.Alt;
            }

            if (key == Keys.ShiftKey || key == Keys.ControlKey || key == Keys.Menu)
            {
                key = Keys.None;
            }

            return modifers;
        }

        public static string GetFormattedKeyPressed(Keys keyData)
        {
            var convertKeys = new KeysConverter();
            return convertKeys.ConvertToInvariantString(keyData);
        }
        #region Namegetters
        public static string GetSkillName(uint _skillid)
        {
            string SkillName = "Skill Not Found";
            if (_skillid == 0)
            {
                SkillName = "None";
            }
            else
            {
                SkillName = _skillid.ToString();
            }
            foreach (var skillname in MainApp.Me.Skills.Where(x => x.Value.Id == _skillid))
            {
                SkillName = skillname.Value.Name;
            }
            return SkillName;
        }

        public static string GetItemName(uint _itemid)
        {
            string ItemName = "Item Not Found";
            if (_itemid == 0)
            {
                ItemName = "None";
            }
            else
            {
                ItemName = _itemid.ToString();
            }
            foreach (var itemname in MainApp.Me.Bags.Where(x => x.Id == _itemid))
            {
                ItemName = itemname.InternalData.Name;
            }
            return ItemName;
        }
        public static string GetEquippedItemName(uint _itemid)
        {
            string EquippedItemName = "Skill Not Found";
            if (_itemid == 0)
            {
                EquippedItemName = "None";
            }
            else
            {
                EquippedItemName = _itemid.ToString();
            }
            foreach (var equipname in MainApp.Me.Bags.Where(x => x.Id == _itemid))
            {
                EquippedItemName = equipname.InternalData.Name;
            }
            return EquippedItemName;
        }
        #endregion
        #endregion
    }

    #region Helpful Classes
    public class KingfischInterfaceObject
    {
        public string Text { get; set; }
        public object Object { get; set; }

        public override string ToString()
        {
            return Text;
        }

    }

    public class YesNoBool
    {

        public bool ImportedBool { get; set; }



        public override string ToString()
        {
            if (ImportedBool == true)
            {
                return "Yes";
            }
            else
            {
                return "No";
            }
        }
    }
    #endregion
}
